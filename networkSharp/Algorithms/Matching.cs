﻿using System;
using System.Collections.Generic;
using System.Text;
using networkSharp.Classes;
using System.Linq;
using System.Diagnostics;


namespace networkSharp.Algorithm
{
    // A Matching in a graph is a subset of edges in which nodes appear no more than once
    // This is just a helper class that holds our matching algorithms
    using Matching = HashSet<Edge>;
    using Nodes = List<Node>;
    using Edges = List<Edge>;

    class MatchingAlgorithms
    {
        // Maximal matching is a matching such that it cannot add any more edges while still being a matching
        public Matching MaximalMatching(Graph g)
        {
            Matching matching = new Matching();
            Nodes nodes = new Nodes();
            foreach (Edge e in g.Edges)
            {
                Node n1 = e.GetNodes().Item1;
                Node n2 = e.GetNodes().Item2;
                if (!(nodes.Contains(n1) || nodes.Contains(n2) || n1 == n2 ) )
                {
                    matching.Add(e);
                    nodes.Add(n1);
                    nodes.Add(n2);
                }
            }
            return matching;
        }

        // Perfect matchings are where every node in the graph appears in the matching
        public bool IsPerfectMatching(Graph g, Matching matching)
        {
            // set up our counter
            Dictionary<Node, int> counts = new Dictionary<Node, int>() { };
            foreach (Node n in g.GetNodes())
            {
                counts.Add(n, 0);
            }
            // work out how many times our nodes appear in the matching
            foreach (Edge e in matching)
            {

                if (counts.ContainsKey(e.GetNodes().Item1)
                    && counts.ContainsKey(e.GetNodes().Item2))
                {
                    counts[e.GetNodes().Item1]++;
                    counts[e.GetNodes().Item2]++;
                }
                else
                {
                    throw new KeyNotFoundException(String.Format("One of the nodes in on the edge was not found in the graph: %s %s", e.GetNodes().Item1, e.GetNodes().Item2));
                }
            }

            foreach (var _ in
            // if any of our counts are not 1, the node is not in the graph
            // or we have a count of two, which shouldn't happen
            from int i in counts.Values
            where i != 1
            select new { })
            {
                return false;
            }

            return true;
        }
        

        // Decides whether a given matching is actually a matching in G
        public bool IsMatching(Matching match, Graph graph)
        {
            Nodes nodeCounts = new Nodes();

            foreach (Edge e in match)
            {
                Node n1 = e.GetNodes().Item1;
                Node n2 = e.GetNodes().Item2;

                
                if    (graph.GetNodes().Contains(n1)
                    && graph.GetNodes().Contains(n2))
                    {
                        if (nodeCounts.Contains(n1) || nodeCounts.Contains(n2)) 
                        { 
                            return false; 
                        }
                        else
                        {
                            nodeCounts.Add(n1);
                            nodeCounts.Add(n2);
                        }
                        
                    }
            }

            return true;
        }

        // Wrapper for lazy folks
        public bool IsMatching(Matching matching)
        {
            return IsMatching(matching, matching.First().GetNodes().Item1.Graph);
        }

        // Constructs a two-way dictionary from our hashset
        // We will have mirrored key/value pairs
        // E.g `u:v` and `v:u`
        public Dictionary<Node,Node> MatchingToDict(Matching matching)
        {
            Dictionary<Node, Node> dict = new Dictionary<Node, Node>();
            foreach (Edge e in matching)
            {
                dict.Add(e.GetNodes().Item1, e.GetNodes().Item2);
                dict.Add(e.GetNodes().Item2, e.GetNodes().Item1);
            }
            return dict;
        }

        public Matching DictToMatching(Dictionary<Node, Node> dict)
        {
            Matching matching = new Matching();
            Graph graph = dict.First().Value.Graph;         // assume our first value has the right graph
            foreach (KeyValuePair<Node,Node> p in dict)
            {
                try
                {
                    Edge e = graph.FindEdge(p.Key, p.Value);
                    if (matching.Contains(e)) { continue; } // maybe throw here?
                    else { matching.Add(e); } 
                }
                catch (KeyNotFoundException)
                {
                    throw new KeyNotFoundException("One of the edges were not in the graph!");
                }
            }

            return matching;
        }


        public bool IsMaximalMatching(Matching matching)
        {
            // If we don't have a matching at all, it's obvs not maximal
            if (!IsMatching(matching)) { return false; }
            Graph graph = matching.First().Nodes.Item1.Graph;

            var unmatchedEdges = graph.Edges.Where(e => !matching.Contains(e));

            foreach (Edge e in unmatchedEdges)
            {
                // TODO:
                // Add/remove feels inefficient
                matching.Add(e);
                if (IsMatching(matching)) 
                { 
                    matching.Remove(e);
                    return false; 
                }
                matching.Remove(e);
            }
            return true;
        }
        
        public Matching MaximalWeightedMatching(Graph graph, bool maxCardinality = false)
        {
            Matching result = new Matching();
            List<Node> nodes = graph.Nodes;
            if (nodes.Count <= 1) { return result; }

            double maxWeight = graph.Edges.OrderByDescending(e => e.Weight).First().Weight;

            // If v is a matched vertex, mate[v] is its partner vertex
            // This goes both ways, so is mate[v] = u, then mate[u] = v
            Dictionary<Node, Node> mates = new Dictionary<Node, Node>();

            Dictionary<Blossom, BlossomLabel> labels = new Dictionary<Blossom, BlossomLabel>();
            
            // Given a labeled top-level blossom b,
            // labelEdges[b] = Edge(v,w) is the edge through which b obtained its label
            // such that w is a vertex in b
            Dictionary<Blossom, Edge?> labelEdges = new Dictionary<Blossom, Edge?>();
            
            // inBlossom[v] gives the top level blossom that contains v
            // Initially there are no blossoms, so the blossom that 
            // contains each node is the node itself
            Dictionary<Node, Blossom> inBlossom = new Dictionary<Node, Blossom>();
            foreach (Node n in graph.Nodes) { inBlossom[n] = n; }

            // If b is a sub-blossom, blossomParent[b] is its immediate parent
            // If b is a top-level blossom, blossomParent[b] == b
            // TODO: Revisit this, it feels bad
            Dictionary<Blossom, Blossom> blossomParent = new Dictionary<Blossom, Blossom>();
            foreach (Blossom b in graph.Nodes) { blossomParent[b] = b; }

            // If b is a blossom, blossomBase is its base vertex
            Dictionary<Blossom, Blossom> blossomBase = new Dictionary<Blossom, Blossom>();
            foreach (Blossom b in graph.Nodes) { blossomBase[b] = b; }

            // If w is a free vertex (or an unreached vertex inside a T-blossom),
            // bestEdges[w] is the least slack edge from an S-vertex,
            // or null if there is no such edge.
            // If b is a top-level S-blossom, besEdges[b] = Edge(v, w) is the least slack 
            // edge to a different S-Blossom (v inside b), or null if there is no such edge
            // Used in computation of delta2 and delta3
            Dictionary<Node, Edge?> bestEdges = new Dictionary<Node, Edge?>();

            // If n is a node,
            // dualVar[n] = 2 * u(n) where u(n) is the n's variable in the dual optimization problem
            // Initially, u(n) == maxweight / 2
            Dictionary<Node, Weight> dualVar = new Dictionary<Node, Weight>();
            foreach (Node n in graph.Nodes) { dualVar[n] = maxWeight; }

            // If b is a non-trivial blossom, blossomDual[b] == z(b)
            // where z(b) is b's variable in the dual optimization problem
            Dictionary<Blossom, Weight> blossomDual = new Dictionary<Blossom, Weight>();

            // If an edge is in allowEdges then the edge is known to have zero slack 
            // in the optimization problem.
            Edges allowEdges = new Edges();

            // Queue of newly discovered S-vertices
            Nodes queue = new Nodes();

            double slack(Node v, Node w) { return dualVar[v] + dualVar[w] - 2 * graph.FindEdge(v, w).Weight; }

            void assignLabel(Node w, Node v, BlossomLabel t)
            {
                Blossom b = inBlossom[w];
                labels[(Blossom)w] = t;
                labels[b] = t;
                if (v is Object) 
                {
                    Edge e = graph.FindEdge(v, w);
                    labelEdges[w] = e;
                    labelEdges[b] = e;
                }
                else
                {
                    labelEdges[w] = null;
                    labelEdges[b] = null;
                }
                bestEdges[w] = null;
                bestEdges[b] = null;

                if (t == BlossomLabel.S_Blossom)
                {
                    // b became an S-blossom, so add it/it's vertices to the queue
                    if (b is Blossom) { queue.AddRange(b.GetLeaves()); }
                    else { queue.Add(b); }
                }
                else if (t == BlossomLabel.T_Blossom)
                {
                    // b became a T-blossom, so assign label S to its mate
                    // If b is a non-trivial blossom, its base is the only vertex
                    // with an external mate
                    Node _base = blossomBase[b];
                    assignLabel(mates[_base], _base, BlossomLabel.S_Blossom);
                }

            }
            
            // this might return null
            // feels bad but the reference does this too, sorta
            Node scanBlossom(Node? v, Node? w)
            {
                Nodes path = new Nodes();
                Node? _base = null;
                while (v is Object)
                {
                    // Look for a breadcrumb in v's blossom
                    Blossom b = inBlossom[v];
                    if (labels[b] & 4)
                    {
                        _base = blossomBase[b];
                        break;
                    }
                    //  or put a new breadcrumb
                    Debug.Assert(labels[b] == 1);
                    path.Append(b);
                    labels[b] = BlossomLabel.Five;
                    // trace one step back
                    if (!labelEdges.ContainsKey(b) || (labelEdges[b] is null))
                    {
                        // The base of b is single, stop tracing this path
                        Debug.Assert(!mate.Contains(blossomBase[b]));
                        v = null;
                    }
                    else
                    {
                        Node n, n1, n2;
                        (n1, n2) = labelEdges[b].Nodes;
                        if (b._childs.Contains(n1)) { n = n2;}
                        else { n = n1; }
                        Debug.Assert(mates[blossomBase[b]] == n);
                        v = n;
                        b = inBlossom[v];
                        Debug.Assert(labels[b] == BlossomLabel.T_Blossom);
                        // b is a T-blossom, trace one more step back
                        (n1, n2) = labelEdges[b].Nodes;
                        if (b._childs.Contains(n1)) { v = n2; }
                        else { v = n1; }
                    }
                    // swap v and w so that we alternate between both paths around the blossom
                    if (w is Object)
                    {
                        Node t = v;
                        v = w;
                        w = t;
                    }
                }
                foreach (Node b in path)
                {

                    labels[b] = BlossomLabel.S_Blossom;
                }
                return _base;
            }
            return result;
        }

        


        // Internal class, representing a blossom or sub-blossom
        private class Blossom: Node
        {
            private Blossom(Graph g): base(g) {}
            // Temporary warning disable while the class is WIP
            #pragma warning disable CS0169
            
            // Ordered list of sub-blossoms, starting with the base 
            // and continuing around the blossom
            // These may be Nodes or Blossoms
            public List<Node> _childs;
            
            // Connecting edges to the blossom, such that _edges[i] = (v, w)
            // where v is a vertex in _childs[i] and w is a vertex in 
            public List<Edge> _edges;
            public List<Edge> _bestEdges; // used for calculating delta3
            #pragma warning restore CS0169
            public IEnumerable<Node> GetLeaves()
            {
                foreach (var b in _childs)
                {
                    if (b is Blossom)
                    {
                        // Nasty cast
                        foreach (var v in ((Blossom)b).GetLeaves())
                        {
                            yield return v;
                        }
                    }
                    else
                    {
                        yield return b;
                    }
                }
            }
        }

        private enum BlossomLabel: int
        {
            Unlabeled = 0,
            S_Blossom = 1,
            T_Blossom = 2,
            // Unsure as to why this needs to be here?
            Three = 3,
            Four = 4,
            Five = 5

        }
    }
}
