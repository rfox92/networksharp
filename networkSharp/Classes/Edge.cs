﻿using System;
using System.Collections.Generic;
using System.Text;

namespace networkSharp.Classes
{
    public class Edge
    {
        #region fields
        private Tuple<Node, Node> _nodes;
        private double _weight = 1;


        #endregion fields
        #region properties
        public Tuple<Node, Node> Nodes { get => _nodes; set => _nodes = value; }
        public double Weight { get => _weight; set => _weight = value; }

        #endregion properties
        #region publicMethods
        public double GetWeight() { return Weight; }
        public void SetWeight(double w) { Weight = w; }

        public Tuple<Node, Node> GetNodes() { return Nodes; }
        public void SetNodes(Node n1, Node n2) { Nodes = new Tuple<Node, Node>(n1, n2); }
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.Append(Nodes.Item1.ToString());
            s.Append(" ");
            s.Append(Nodes.Item2.ToString());
            
            if (Weight != 0)
            {
                s.Append(" ");
                s.Append(Weight.ToString());
            }

            return s.ToString();
        }
        #endregion publicMethods

        #region constructors
        public Edge(Node n1, Node n2)
        {
            this.SetNodes(n1, n2);
        }
        public Edge(Node n1, Node n2, double w)
        {
            this.SetNodes(n1, n2);
            this.SetWeight(w);
        }
        #endregion constructors
    }
}
