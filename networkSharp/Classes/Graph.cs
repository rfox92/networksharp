﻿using System.Collections.Generic;
using System.Linq;

namespace networkSharp.Classes
{
    
    using Nodes = List<Node>;
    using Edges = HashSet<Edge>;
    
    public class Graph
    {
        #region fields
        private Nodes _nodes;
        private Edges _edges;
        #endregion fields
        #region properties
        public Nodes Nodes { get => _nodes; }
        public Edges Edges { get => _edges; }
        #endregion properties
        #region publicMethods
        public void Clear()
        {
            this.ClearNodes();
            this.ClearEdges();
        }
        public void ClearNodes()
        {
            _nodes.Clear();
        }
        public void ClearEdges()
        {
            _edges.Clear();
        }
        public Nodes GetNodes() => Nodes;
        public void AddNode() => Nodes.Add(new Node(this));
        public void AddNode(string name) => Nodes.Add(new Node(this, name));
        public void AddEdge(Node n1, Node n2)
        {
            if ((n1.Graph == n2.Graph) && (n1.Graph == this))
            {
                _edges.Add(new Edge(n1, n2));
            }
            else throw new KeyNotFoundException("Both nodes must be in the same graph.");
        }
        public void AddEdge(Node n1, Node n2, double weight)
        {
            if ((n1.Graph == n2.Graph) && n1.Graph == this)
            {
                _edges.Add(new Edge(n1, n2, weight));
            }
            else throw new KeyNotFoundException("Both nodes must be in the same graph.");
        }
        public void RemoveEdge(Node n1, Node n2)
        {
            // if our edge don't exist, return out
            // also prevents FindEdge from throwing
            if (!HasEdge(n1, n2)) { return; }
            _edges.Remove(FindEdge(n1, n2));
        }
        public bool HasEdge(Node n1, Node n2)
        {
            // if we've got nodes from different graphs, they can't be connected
            
            if (   n1.GetGraph() != this
                || n2.GetGraph() != this)
            {
                throw new KeyNotFoundException("Both nodes must be in this graph.");
            }
            
            var m = _edges.Where(e => 
                       (e.Nodes.Item1 == n1 && e.Nodes.Item2 == n2) 
                    || (e.Nodes.Item1 == n2 && e.Nodes.Item2 == n1));
                       
            return m.Count() > 0;
        }
        public Edge FindEdge(Node n1, Node n2)
        {
            var m = _edges.Where(e => 
                       (e.Nodes.Item1 == n1 && e.Nodes.Item2 == n2) 
                    || (e.Nodes.Item1 == n2 && e.Nodes.Item2 == n1));
            if (m.Count() > 0) { return m.First(); }
            // if we can't find our edge, throw
            throw new KeyNotFoundException(string.Format("No matching edges between nodes %s and %s",n1.Name, n2.Name));
        }

        public Edges GetEdges()
        {
            return _edges;
        }


        #endregion publicMethods
        #region constructors
        public Graph()
        {
            _nodes = new Nodes();
            _edges = new Edges();
        }
        #endregion constructors
    }
}
