﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace networkSharp.Classes
{
    public class Node
    {
        #region fields
        private readonly Graph _graph;
        private string _name = "";
        #endregion fields
        #region properties
        public string Name { get => _name; set => _name = value; }
        public Graph Graph => _graph;
        public List<Edge> Edges
        {
            get
            {
                new List<Edge>().AddRange(from Edge e in this.Graph.GetEdges()
                                          where e.GetNodes().Item1 == this || e.GetNodes().Item2 == this
                                          select e);
                return new List<Edge>();
            }
        }
        #endregion properties
        #region publicMethods
        public void AddEdge(Node destination) => Graph.AddEdge(this, destination);
        public void AddEdge(Node destination, double weight) => Graph.AddEdge(this, destination, weight);
        public void RemoveEdge(Node destination) => Graph.RemoveEdge(this, destination);


        public Graph GetGraph() => Graph;

        #endregion publicMethods
        #region constructors
        public Node(Graph g) => _graph = g;
        public Node(Graph g, string name)
        {
            _graph = g;
            _name = name;
        }
        #endregion constructors
        #region destructors
        #endregion destructors
    }
}
