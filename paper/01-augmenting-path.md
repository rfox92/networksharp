## Augmenting Paths

- An Augmenting path is a path where:
  - Alternating edges are matched
  - The path starts and ends with an unmatched node
  - The length is odd

- We can use augmenting paths to perform maximal matchings in bipartite graphs - if we have an augmenting path, and we change the state of the edges (unmatched -> matched and vice versa), then we increase the size of our matching by 1.

- Consider the following bipartite graph:

![](img/01-augmenting-bipartite-stage-0.png)

- We match an arbitrary edge (`|M| = 1`):

![](img/01-augmenting-bipartite-stage-01.1.png)

- We then create an augmenting path:

![](img/01-augmenting-bipartite-stage-01.2.png)

- And change the state of the edges (`|M| = 2`)

![](img/01-augmenting-bipartite-stage-01.3.png)

- We create another augmenting path:

![](img/01-augmenting-bipartite-stage-02.2.png)

- Again, change the state of the edges (`|M| = 3`)

![](img/01-augmenting-bipartite-stage-02.3.png)

- Finally, we can only create an alternating path of length 1:

![](img/01-augmenting-bipartite-stage-03.2.png)

- So we change the state of this edge, and our matching is maximal (`|M| = 4`)

![](img/01-augmenting-bipartite-stage-03.3.png)